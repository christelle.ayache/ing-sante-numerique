---
title: Programme
slug: programme
description: null
draft: false
menu:
  main:
    name: Programme
    weight: 999
url: /programme/
---

# 1er rencontre à Rennes (2022)

(*ce programme est susceptible aux changements pendant les prochains jours*)

*Dates :* mercredi 7 et vendredi 8 décembre

*Lieu :* Inria de l'université de Rennes

## Jour 1

- 10h00-10h30: Accueil
- 10h30-11h00: Tour de table
- 11h00-12h00: Activités *Santé Numérique (SN)* par centre.
    - [Lyon (Antoine Fraboulet)](./presentations/SN-Lyon.pdf)
    - [Rennes (Eric Poiseau)](./presentations/SN-Rennes.pdf)
    - [Bordeaux (Dan Dutartre)](./presentations/SN-Bordeaux.pdf)
    - [Saclay (Benjamin Nguyen-Van-Yen)](./presentations/SN-Saclay.pdf)
    - Paris (Pierre-Guillaume Raverdy)
- 12h00-13h30: Déjeuner
- 13h30-15h30: Présentations de projets, partie 1.
    - [Primetime IRE (Luc Laffite)](./presentations/primetime.pdf)
    - [RadioNLP (Salim Sadoune)](./presentations/radionlp.pdf)
    - [OpenViBE (Thomas Prampart)](./presentations/openvibe.pdf)
    - [Shanoir (Michael Kain)](./presentations/shanoir.pdf)
    - [HappyFeat (Arthur Desbois)](./presentations/happyfeat.pdf)
    - [Fed-BioMed (Yannick Bouillard, Francesco Cremonesi)](./presentations/fedbiomed.pdf)
    - [Apprentissage Fédéré : Seclearn + projets en partenariat avec les CHU de Lille, Amiens, Caen et Rouen" (Paul Andrey)](./presentations/declearn.pdf)
    - [AIstroSight et HyperFlow (Jan-Michael Rye)](./presentations/aistrosight.pdf)
- 15h30-15h45: Pause café.
- 15h45-17h45: Présentations de projets, partie 2.
    - [Clinica (Nicolas Gensollen)](./presentations/clinica.pdf)
    - [ClinicaDL (Camille Brianceau)](./presentations/clinicadl.pdf)
    - [MedInria (Julien Castelneau)](./presentations/medinria.pdf)
    - [BioImageIT: Integration of data-management with analysis (Leo Maury)](./presentations/bioimageit.pdf)
    - [MedKit (Olivier Birot)](./presentations/medkit.pdf)

## Jour 2

- 08h00-08h15: Accueil
- 08h15-10h15: Session de travail 1, **Gestion de données**
    - Formats: présentations introductoires [DICOM (Eric Poiseau)](./presentations/dicom.pdf), [BIDS (Ghislain Vaillant)](https://aramis-lab.github.io/idhm2022-bids-primer/).
    - Accès: PACS, Shanoir
    - [Plateformes de données: SNDS, HDH, PDS-Inria (Kim-Tâm Huynh, rSEDs)](./presentations/plateforme.pdf)
    - Systèmes de contôle de version pour les données.
    - Anonimisation, pseudonimisation
- 10h15-10h30: Pause café
- 10h30-11h15 Présentation de projets, partie 3.
    - [URGE "Analyse des parcours patients aux urgences et optimisation des prises en charge" (Benjamin Nguyen-Van-Yen)](./presentations/urge.pdf)
    - [Gnomon (Tristan Cabel)](./presentations/gnomon.pdf)
    - [Vidjil (Florian Thonier)](./presentations/vidjil.pdf)
- 11h15-12h30: Session de travail 2, **Chaine de traitement pour le dev. logiciel**
    - Comment les données sont intégrées à la chaîne de traitement (cas spécial pour le traitement féderé) ?
    - Outils pour la chaîne de traitement: [Nypipe/Pydra (Matthieu Joulot - Ghislain Vaillant)](https://github.com/aramis-lab/idhm2022-pydra-demo).
    - Déploiement de résultats (données, modèles, documentation, tutoriaux). Demostrateurs (e.g., [A||go](https://allgo18.inria.fr/)).
    - Tests, CI/CD, outils MLOps.
