---
title: Programme
slug: programme
description: null
draft: false
menu:
  main:
    name: Programme
    weight: 999
url: /programme/
---

# 3éme rencontre à Rennes (2023)

(*ce programme est susceptible aux changements*)

*Dates :* mardi 26 à jeudi 28 septembre

*Lieu :* Centre Inria de l'université de Rennes et le Couvent des Jacobins

## Jour 1 (mardi 26/09, après-midi, lieu: [Pôle numérique Rennes Beaulieu (PNRB)](https://metropole.rennes.fr/organisme/pole-numerique-rennes-beaulieu-universite-loire-bretagne-6266), Université de Rennes - Campus Beaulieu)

* 13h30 - 14h00  Accueil 
* 14h00 - 17h30  Demie-journée des données et de la gouvernance (présentation de 20 à 30 minutes suivies de discussion):
    * 14h00 - 14h45  [La plateforme de données sensibles à Inria](./presentations/2023-09-26-Rennes-PDS.pdf). **Antoine Fraboulet** (Responsable du SED Lyon) - En visio
    * 14h45 - 15h30  [Tracer la provenance des données de neuroimagerie: est-ce vraiment utile ?](./presentations/Maumet.pdf) **Camille Maumet** (Chercheuse, équipe-projet : Empenn) 
    * 15h30 - 16h45  [Protection juridique et technique des données de santé](./presentations/Protection_juridique_et_technique_des_données_de_santé.pdf). **Dominique Launay** (RSSI Inria), **Emilie Masson** (DPO Adjointe Inria), **Anne Combe** (DPO Inria).
    * 16h45 - 17h30  [Retour d'expérience sur l'utilisation de données du SNDS](./presentations/retex_donnees_SNDS.pdf). **Octave Guinebretiere** (Doctorant, équipe-projet : Aramis). 

## Jour 2 (mercredi 27/09, matin, [Centre Inria de l'Université de Rennes](https://www.inria.fr/fr/centre-inria-universite-rennes))

* 9h00 - 12h00 Demie-journée sur l'interoperabilité
    * 9h00 - 10h00 [Présentation du Projet Inria Hospital Suite](./presentations/InriaDigitalHealth.pdf), **Marco Lorenzi** (Chercheur, équipe-projet : Epione)
    * 10h00 - 11h30 Interoperabilité logiciels "Inria" : état d'avancement
        - Shanoir, **Michael Kain** (Ingénieur au SED Rennes) 
        - MedInria, **Florent Leray**, **Julien Castelneau** (Ingénieurs au SED Rennes/Bordeaux) 
        - FedBiomed, **Sergen Cansiz**, **Yannick Bouillard** (Ingénieurs au SED Sophia)
    * 11:30 - 12h15 Pitch sur d'autres logiciels de l'ecosystème Inria
        - [Medkit](./presentations/IDHM_2023_-_medkit.pdf), **Ghislain Vaillant** (Ingénieur au SED Paris)
        - Chemfeat, **Jan-Michael Rye** (Ingénieur au SED de Lyon)
        - MoReFEM, **Sébastien Gilles** (Responsable du SED Saclay)


## Jour 2 (mercredi 27/09, après-midi, [Centre Inria de l'Université de Rennes](https://www.inria.fr/fr/centre-inria-universite-rennes))

* 14h00 - 17h30 Demie-journée de discussion :  tables rondes/ateliers autour de l'interoperabilité (ouvert à propositions)
    * Partage de données / Formats d'échanges
    * IA 
    * API / Interconnexion des process
* Retour / Synthèse des ateliers

## Jour 3 (jeudi 28/09, matin, au [Couvent des Jacobins](https://www.centre-congres-rennes.fr/fr/))

* 9h00 - 12h00 Les projets structurants / stratégiques
    * Meditwin - 3DS **Guillaume Turpin** (Responsable de Partenariats Stratégiques, DGDI)
    * PEPR Santé Numérique, **Philippe Gesnouin**  (Responsable du programme Santé Numérique, DGDI)
* Démos Techno Inria
